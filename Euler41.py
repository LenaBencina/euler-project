__author__ = 'Lena'

from math import sqrt
from itertools import count, islice, permutations

def isPrime(n): #check if n is a prime number
    if n < 2: return False
    for number in islice(count(2), int(sqrt(n)-1)):
        if not n%number:
            return False
    return True


result = 0
length = 7 #result is a 7-digit number
first_number = [i for i in range(1,length+1)]
first_number = int(''.join(map(str,first_number)))
perm = [''.join(p) for p in permutations(str(first_number)) if int(''.join(p)) % 2 != 0 and p[len(p)-1] != 5]
for number in perm:
    number = int(number)
    if isPrime(number):
        if number > result:
            result = number
print(result)