__author__ = 'Lena'

import string
import re

alphabet = string.ascii_uppercase
with open('names.txt', 'rt') as names:
    names = names.read()
    names = names.strip('"')
    names = re.split('","',names)
    names = sorted(names, key=str.lower)
    result,counter = 0,0
    for name in names:
        counter += 1
        name_value = 0
        for character in name:
            name_value += (alphabet.index(character) + 1)
        name_value *= counter
        result += name_value
